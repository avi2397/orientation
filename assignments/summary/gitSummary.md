# Git Summary


#### Version Control System (VCS)

Version control is a system that records changes to a file or set of files over time so that you can recall specific versions later. 

![VCS](https://git-scm.com/book/en/v2/images/local.png)

---


#### What is git

Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.

---

#### Basics of git

Git has three main states that your files can reside in: modified, staged, and committed:
* **Modified** means that you have changed the file but have not committed it to your database yet.
* **Staged** means that you have marked a modified file in its current version to go into your next commit snapshot.
* **Committed** means that the data is safely stored in your local database.


![git stages](https://git-scm.com/book/en/v2/images/areas.png)

---


#### Basc workflow of git

The basic Git workflow goes something like this:
* You modify files in your working tree.
* You selectively stage just those changes you want to be part of your next commit, which adds only those changes to the staging area.
* You do a commit, which takes the files as they are in the staging area and stores that snapshot permanently to your Git directory.


---


#### Basic git commands

1. **configuration**
    * git config --global user.name "Sam Smith"
    * git config --global user.email sam@example.com
2. **Create a new local repository**
    * git init
3. **Adding files**
    * git add "filename"
    * git add . 
4. **Commiting changes**
    * git commmit -m "message"
5. **Push the commited files**
    * git push origin master
6. **checking status**
    * git status
 
 ---

